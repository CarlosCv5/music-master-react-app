import React, { Component } from 'react';
import Artist from './Artist';
import Search from './Search';
import Tracks from './Tracks';

const API_ADDRESS = 'https://spotify-api-wrapper.appspot.com'

class App extends Component {

    state = { artistInfo : null , tracks : []};

    
    componentDidMount(){
        this.searchArtist('bruno mars');
    }

    searchArtist = artistQuery =>{
        //console.log('this state',this.state.artistQuery);
        fetch(`${API_ADDRESS}/artist/${artistQuery}`)
        .then(response =>response.json())
        .then(json => {

            if (json.artists.total > 0){

                

                const artist = json.artists.items[0];

                this.setState({artistInfo:artist});
                
                fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                .then(response => response.json())
                .then(json => this.setState({tracks : json.tracks}))
                .catch(error => alert(error.message));
            }


        })
        .catch(error => alert(error.message));
    
        
        
    }
    

    


    render() {
        
        console.log("state", this.state)
        
        return(
            <div>
                <h2>Musica Master APP</h2>

                <Search searchArtist = {this.searchArtist}/>

                <Artist artist = {this.state.artistInfo}/>

                <Tracks tracks ={this.state.tracks}/>
                
                

            
            </div>
        )
    }
}

export default App;