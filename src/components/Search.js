import React, { Component } from 'react';

class Search extends Component {

    state={artistQuery:''};

    updateQueryList = event => {
        
        this.setState({artistQuery:event.target.value});

    }

    searchArtist = () => {
        this.props.searchArtist(this.state.artistQuery);
    }

    render(){
        return(
            <div>
              <input
                    onChange = {this.updateQueryList}
                    placeholder = 'Search for an Artist'
                              
                />

                <button onClick={this.searchArtist}>Search</button>    
            </div>
        )
    }
}

export default Search;